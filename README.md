## Contents of this file

- About
- Requirements
- Generating a new theme
- Use / Commands
- Maintainers
- Contributing

## About

Prototype is SDC based theme starterkit for Drupal 10.1+.

This README describes how to generate a new theme using the generator script. For more information on the theme itself, see the [README_STARTERKIT.md](./README_STARTERKIT.md).

## Requirements

- Drupal Core 10.1+

## Generating a New Theme
Install this theme as you would any other Drupal theme. It is not necessary to enable the theme. Once installed, you can generate a new theme using the generator script.

1. Navigate into the `prototype` theme folder
2. Run the generator script with the desired options

```bash
php generator.php -n custom_theme -d 'Custom Theme' -p themes -a short
```

### Generator Options:
```
 -n, --name           The machine name for the theme
 -d, --display-name   The human readable display name for your theme
 -p, --path           The Drupal root relative path where your theme will be created
                        Defaults: 'themes/custom'
 -a, --abbreviated    Use abbreviated theme name for CSS variables. If not provided, the full theme machine name will be used.
```

## Maintainers

- Jennifer Dust - jennifer@aten.io
- Philip Stier - philip@aten.io
- Bryon Urbanec - bryon@aten.io
- John Ferris - john@aten.io

## Contributing

All contributions must have an [corresponding issue](https://www.drupal.org/project/issues/prototype?categories=All) and be made through merge requests.

This theme leverages Drupal.org's [Tugboat QA integration](https://www.drupal.org/docs/develop/git/using-git-to-contribute-to-drupal/using-live-previews-on-drupal-core-and-contrib-merge-requests) to ensure that all code is reviewed and tested before merging. When a merge request is opened, Tugboat will automatically create a preview environment for the merge request. This environment will be available for 5 days. If a preview environment needs to be recreated after that period, the merge request will need to be closed and reopened.

The preview environment has both the [Styleguide](https://www.drupal.org/project/styleguide) and [Storybook](https://www.drupal.org/project/storybook) modules enabled for previewing components. Each of these can be viewed at `/admin/appearance/styleguide` and `/storybook` respectively.
