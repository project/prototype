<?php

namespace Drupal\prototype;

use Drupal\Core\Theme\StarterKitInterface;

final class StarterKit implements StarterKitInterface {

  /**
   * Helper function to remove a directory and its contents.
   *
   * @param string $dir Path to the directory to remove.
   *
   * @return void
   */
  private static function removeDirectory(string $dir): void {
    if (!is_dir($dir)) {
      return;
    }
    $items = new \RecursiveIteratorIterator(
      new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS),
      \RecursiveIteratorIterator::CHILD_FIRST
    );
    foreach ($items as $item) {
      $item->isDir() ? rmdir($item->getRealPath()) : unlink($item->getRealPath());
    }
    rmdir($dir);
  }

  /**
   * {@inheritdoc}
   */
  public static function postProcess(string $working_dir, string $machine_name, string $theme_name): void {
    //Path to new theme info file.
    $info_file = "$working_dir/$machine_name.info.yml";

    try {
      $file_contents = file_get_contents($info_file);
       // Remove 'starterkit: true' line.
      $file_contents = str_replace('starterkit: true' . "\n", '', $file_contents);
      $file_contents = str_replace('enforce_sdc_schemas: true' . "\n", '', $file_contents);
      // Replace 'generator: $theme_name' with 'generator: prototype'.
      $file_contents = preg_replace("/(generator: )$theme_name/", '$1prototype', $file_contents);
      file_put_contents($info_file, $file_contents);

      // Remove the tests and node_modules folders if it exists since ignoring it
      // doesn't seem to work as expected.
      $directories_to_remove = [
        "$working_dir/tests",
        // If we don't delete node_modules, it will be copied to the new theme
        // and all instances of the native JS 'prototype' keyword will get
        // replaced with the theme name which is very much not a good thing.
        "$working_dir/node_modules",
      ];
      foreach ($directories_to_remove as $folder) {
        if (is_dir($folder)) {
          self::removeDirectory($folder);
        }
      }

      // Rename the STARTEKIT.md file to README.md.
      $readme_file = "$working_dir/README_STARTERKIT.md";
      $readme_file_new = "$working_dir/README.md";
      if (file_exists($readme_file)) {
        rename($readme_file, $readme_file_new);
      }
    }
    catch (\Throwable $th) {
    }
  }
}
