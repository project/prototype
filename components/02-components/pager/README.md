# Pager Component
The Pager component is used for applying markup, basic style, and functionality, in the form of a pager
element.

## Overview
Pagers are UI components that allow you to page through a number of items in a listing. This Pager  component is
designed to be accessible, ensuring usability for both mouse and keyboard users.

## Installation
To install this component, make sure to place the component directory within Drupal's SDC `/component` directory.

## Usage
To use this component:
1. Include it within a content template using TWIG's include or embed functions.
2. Appropriately fill in the necessary props or slots.

**Example**
```
{% include 'prototype:pager' with {
  attributes: create_attribute().merge(attributes),
  items: items|default([]),
  ellipses: ellispses|default([]),
  current: current,
} only %}
```

## Properties
- `attributes`: Additional HTML attributes for the accordion element.
- `items`: The full collection of elements needed to make up this component.
- `ellipses`: A group of items representing continuations of pagination.
- `current`: The number representative of the current page.

## Slots
- `pager`: The entire pager element.

## Accessibility
The Accordion component is designed to be accessible for both mouse users and keyboard users, as described by as
described by WCAG: https://design-system.w3.org/components/pagination.html. Keyboard users can navigate through the
pager using the tab and arrow keys, ensuring a smooth user experience.
