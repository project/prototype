/**
 * @file
 * Provides Accessible Accordion functionality.
 * Functionality for an Accordion or group of Accordions using accessible methods
 * as described by WCAG: https://www.w3.org/WAI/ARIA/apg/patterns/accordion/
 */

((Drupal, once) => {
  /**
   * Initializes an accessible accordion component.
   *
   * The function takes a wrapper element containing the relevant content and trigger
   * for the accordion and sets up event listeners to handle interactions.
   *
   * The accordion toggles between open and closed states, updating both visual and
   * accessibility-related attributes.
   *
   * @param {Object} wrapper - The accordion wrapper element containing trigger and content
   * elements.
   */
  const a11yAccordion = (wrapper) => {
    const openedClass = 'is-open';
    let content;
    let trigger;

    /**
     * Expands the accordion section by adding the expanded CSS class
     * to the content and trigger elements, and sets the aria-expanded
     * attribute to true.
     *
     * This function modifies the DOM to visually and semantically
     * indicate that the accordion content is currently expanded.
     *
     * @function expand
     * @see onClickTrigger
     * @see onKeydownTrigger
     */
    const open = () => {
      content.classList.add(openedClass);
      trigger.classList.add(openedClass);
      trigger.setAttribute('aria-expanded', 'true');
    };

    /**
     * Collapses an accordion section.
     *
     * This function removes the `openedClass` from both the
     * accordion content and the accordion trigger elements.
     * Additionally, it sets the `aria-expanded` attribute of the
     * accordion trigger to 'false', indicating that the section is collapsed.
     *
     * @function collapse
     * @see onClickTrigger
     * @see onKeydownTrigger
     */
    const close = () => {
      content.classList.remove(openedClass);
      trigger.classList.remove(openedClass);
      trigger.setAttribute('aria-expanded', 'false');
    };

    /**
     * Event handler function for handling click events on an accordion trigger element.
     *
     * This function toggles the state of the accordion element. If the element
     * already has the class `openedClass`, it will call the `close` function to close the
     * accordion. Otherwise, it will call the `open` function to open the accordion.
     *
     * @function onClickTrigger
     * @see init
     */
    const onClickTrigger = () => {
      // Check to see if class exists first.
      if (trigger.classList.contains(openedClass)) {
        close();
      } else {
        open();
      }
    };

    /**
     * Event handler for `keydown` events on a specific trigger element.
     *
     * This function is designed to handle keyboard interactions for improved accessibility,
     * specifically listening for the "Enter" and "Space" keys. When either of these keys
     * is pressed, it prevents the default action and toggles a component's state between
     * open and closed.
     *
     * @function onKeydownTrigger
     * @param {KeyboardEvent} event - The keyboard event object.
     * @see init
     */
    const onKeydownTrigger = (event) => {
      if (event.key === 'Enter' || event.key === ' ') {
        event.preventDefault();
        if (trigger.classList.contains(openedClass)) {
          close();
        } else {
          open();
        }
      }
    };

    /**
     * Initializes the accordion component by setting up event listeners for trigger elements.
     *
     * @function init
     * @param {Object} element - The accordion wrapper element containing the trigger and
     * content.
     */
    const init = (element) => {
      trigger = element.querySelector('.c-accordion__trigger');
      content = element.querySelector('.c-accordion__content');
      trigger.addEventListener('click', onClickTrigger);
      trigger.addEventListener('keydown', onKeydownTrigger);
    };

    // Final Program Run
    init(wrapper);
  };

  Drupal.behaviors.prototypeAccordion = {
    attach(context) {
      const accordion = once('prototype-accordion', context.querySelectorAll('.c-accordion'));

      if (accordion.length !== 0) {
        accordion.forEach((element) => {
          a11yAccordion(element);
        });
      }
    },
  };
})(Drupal, once);
