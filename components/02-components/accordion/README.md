# Accordion Component
The Accordion component is used for applying markup, basic style, and functionality, in the form of an accordion
element.

## Overview
Accordions are UI components that allow you to show and hide sections of related content on a page. This Accordion
component is designed to be accessible, ensuring usability for both mouse and keyboard users.

## Installation
To install this component, make sure to place the component directory within Drupal's SDC /component directory.

## Usage
To use this component:
1. Include it within a content template using TWIG's include or embed functions.
2. Appropriately fill in the necessary props or slots.

**Example**
```
{% embed 'prototype:accordion' with {
attributes: create_attribute().merge(attributes.addClass(classes)),
id: paragraph.id(),
heading: content.field_heading|render ? content.field_heading,
content: content.field_content|render ? content.field_content,
} only %}
{% block content %}
{{ content }}
{% endblock %}
{% endembed %}
```

## Properties
- `attributes`: Additional HTML attributes for the accordion element.
- `id`: The unique identifier for the accordion.
- `heading`: The heading content of the accordion.

## Slots
- `trigger`: The element that triggers the expand/collapse action.
- `content`: The main content of the accordion.

## Accessibility
The Accordion component is designed to be accessible for both mouse users and keyboard users, as described by as
described by WCAG: https://www.w3.org/WAI/ARIA/apg/patterns/accordion/. Keyboard users can navigate through the
accordion using the tab and arrow keys, ensuring a smooth user experience.