/**
 * @file
 * Components - Tabs Content
 */

((Drupal, once) => {
  /**
   * Responsive Tabs
   *
   * A function to implement a responsive tab and accordion system. It dynamically handles the state
   * and interaction of triggers and content panels based on the viewport size, switching between
   * tabbed navigation for larger screens and accordion behavior for smaller screens. The
   * integration includes support for accessibility features like keyboard navigation, ARIA
   * attributes management, and visual cues.
   * @param {Object} wrapper - The container element that wraps the entire tab/accordion
   * system.
   * @param {Number} breakpoint - The pixel width at which the interface switches between tabbed
   * navigation and full accordion mode.
   */
  const responsiveTabs = (wrapper, breakpoint) => {
    const expandedClass = 'expanded';
    let mediaQuery;
    let navigation;
    let groups;
    let state;
    let triggers;

    /**
     * Trigger Collapse
     * Any actions needing to be taken when considering a trigger as collapsed.
     * @param {Object} element - Element to take action upon.
     * @see onTriggerClick
     * @see onTriggerKeydown
     * @see onMediaQueryChange
     */
    const collapseTrigger = (element) => {
      element.classList.remove(expandedClass);
      element.setAttribute('aria-selected', 'false');
      element.setAttribute('aria-expanded', 'false');

      if (state === 'desktop') {
        element.setAttribute('tabindex', '-1');
      }
    };

    /**
     * Trigger Expand
     * Any actions needing to be taken when considering a trigger to be expanded.
     * @param {Object} element - Element to take action upon.
     * @see onTriggerClick
     * @see onTriggerKeydown
     * @see onMediaQueryChange
     */
    const expandTrigger = (element) => {
      element.classList.add(expandedClass);
      element.setAttribute('aria-selected', 'true');
      element.setAttribute('aria-expanded', 'true');

      if (state === 'desktop') {
        element.removeAttribute('tabindex');
      }
    };

    /**
     * Trigger Click
     * Any events needing to be taken when a trigger has been clicked.
     * @param {Object} event - All associated details pertaining to a specific event.
     * @see init
     */
    const onTriggerClick = (event) => {
      const controlID = event.currentTarget.getAttribute('aria-controls');
      const group = wrapper.querySelector(`#${controlID}`);

      if (state === 'desktop') {
        if (
          !event.currentTarget.classList.contains(expandedClass) &&
          !group.classList.contains(expandedClass)
        ) {
          // First set all triggers to be collapsed.
          triggers.forEach((trigger) => {
            collapseTrigger(trigger);
          });

          // First set all panels to be collapsed.
          groups.forEach((element) => {
            element.classList.remove(expandedClass);
          });

          // Finally open current trigger and associated panel.
          expandTrigger(event.currentTarget);
          group.classList.add(expandedClass);
        }
      }

      if (state === 'mobile') {
        if (event.currentTarget.classList.contains(expandedClass)) {
          // Collapse accordion.
          collapseTrigger(event.currentTarget);
          group.classList.remove(expandedClass);
        } else {
          // Expand accordion.
          expandTrigger(event.currentTarget);
          group.classList.add(expandedClass);
        }
      }
    };

    /**
     * Trigger Keydown
     * Any events needing to be taken when a trigger has been interacted with via keyboard.
     * @param {Object} event - All associated details pertaining to a specific event.
     * @see init
     */
    const onTriggerKeydown = (event) => {
      const controlID = event.currentTarget.getAttribute('aria-controls');
      const group = wrapper.querySelector(`#${controlID}`);

      if (state === 'desktop') {
        if (event.key === 'ArrowLeft') {
          // Set focus to last trigger if on the first.
          // Otherwise, set focus to the trigger to the left.
          if (event.currentTarget === triggers[0]) {
            setTimeout(() => {
              triggers[triggers.length - 1].focus();
            }, 1);
          } else {
            setTimeout(() => {
              event.target.previousElementSibling.focus();
            }, 1);
          }
        }

        if (event.key === 'ArrowRight') {
          // Set focus to first trigger if on the last.
          // Otherwise, set focus to the trigger to the right.
          if (event.currentTarget === triggers[triggers.length - 1]) {
            setTimeout(() => {
              triggers[0].focus();
            }, 1);
          } else {
            setTimeout(() => {
              event.target.nextElementSibling.focus();
            }, 1);
          }
        }

        if (event.key === 'Home') {
          setTimeout(() => {
            triggers[0].focus();
          }, 1);
        }

        if (event.key === 'End') {
          setTimeout(() => {
            triggers[triggers.length - 1].focus();
          }, 1);
        }
      }

      if (event.key === 'Enter' || event.key === ' ') {
        event.preventDefault();

        if (state === 'desktop') {
          if (
            !event.currentTarget.classList.contains(expandedClass) &&
            !group.classList.contains(expandedClass)
          ) {
            // First set all triggers to be collapsed.
            triggers.forEach((trigger) => {
              collapseTrigger(trigger);
            });

            // First set all panels to be collapsed.
            groups.forEach((element) => {
              element.classList.remove(expandedClass);
            });

            // Finally open current trigger and associated panel.
            expandTrigger(event.currentTarget);
            group.classList.add(expandedClass);
          }
        }

        if (state === 'mobile') {
          if (event.currentTarget.classList.contains(expandedClass)) {
            // Collapse accordion.
            collapseTrigger(event.currentTarget);
            group.classList.remove(expandedClass);
          } else {
            // Expand accordion.
            expandTrigger(event.currentTarget);
            group.classList.add(expandedClass);
          }
        }
      }
    };

    /**
     * Media Query Change
     * Any events needing to be taken when a specific breakpoint has been activated.
     * @see init
     */
    const onMediaQueryChange = () => {
      if (mediaQuery.matches) {
        state = 'desktop';

        // Grab all triggers from within panels and move them to the navigation.
        // Also reset the first panel to be active.
        triggers.forEach((trigger, index) => {
          navigation.append(trigger);

          if (index !== 0) {
            collapseTrigger(trigger);
            groups[index].classList.remove(expandedClass);
          } else {
            expandTrigger(trigger);
            groups[0].classList.add(expandedClass);
          }
        });
      } else {
        state = 'mobile';

        // Grab all triggers from within the navigation and move them to respective panels.
        triggers.forEach((trigger, index) => {
          groups[index].prepend(trigger);
          trigger.removeAttribute('tabindex');
        });
      }
    };

    /**
     * Initialization
     * Add any and all functionality as a singular program, dynamically setting
     * element variables, states and event listeners.
     * @param {Object} element - Element that contains all elements necessary
     * @param {Number} bp -  Numeric value representing the pixel width at which to switch
     * tabs and accordions.
     * for responsive tabs functionality.
     */
    const init = (element, bp) => {
      navigation = element.querySelector('.c-tabs-content__navigation');
      groups = element.querySelectorAll('.c-tabs-content__group');
      triggers = element.querySelectorAll('.c-tabs-content__trigger');

      triggers.forEach((trigger) => {
        trigger.addEventListener('click', onTriggerClick);
        trigger.addEventListener('keydown', onTriggerKeydown);
      });

      // Device or viewport change listener and on load.
      mediaQuery = window.matchMedia(`(min-width: ${bp}px)`);
      mediaQuery.addEventListener('change', onMediaQueryChange);
      onMediaQueryChange();
    };

    init(wrapper, breakpoint);
  };

  /**
   * Provides behavior for handling tabs content in a prototype.
   *
   * This Drupal behavior is attached to dynamically handle and modify
   * the content of tabs on a page when the page or specific elements
   * are loaded. It ensures appropriate tabs content interaction within
   * the context of a Drupal site.
   *
   * Drupal behaviors are essential for attaching JavaScript functionality
   * and ensuring it works consistently with Drupal's theming and AJAX updates.
   *
   * @namespace
   * @property {Object} prototypeTabsContent - The behavior name for managing tabs content.
   * @function attach
   * @param {Object} context - The DOM element or document being processed.
   * @param {Object} settings - A collection of all Drupal settings, including custom and
   * theme-specific settings.
   */
  Drupal.behaviors.prototypeTabsContent = {
    attach(context) {
      const tabs = once(
        'prototype-tabs-content',
        context.querySelectorAll('.c-tabs-content'),
      );

      if (tabs.length !== 0) {
        tabs.forEach((tab) => {
          const breakpoint = tab.getAttribute('data-tabs-content-breakpoint');
          responsiveTabs(tab, breakpoint);
        });
      }
    },
  };
})(Drupal, once);
