/**
 * @file
 * Components - Slider
 * Establishing a working library in order to attach the Splide slider
 * JavaScript library wherever needed. Splide controls, structure and other
 * documentation can be found at https://splidejs.com/guides/getting-started/
 */

((Drupal, once) => {
  /* eslint-disable */
  Splide.defaults = {
    i18n: {
      prev: 'Previous slide',
      next: 'Next slide',
    },
  };
  /* eslint-enable */

  /**
   * Defines a Drupal behavior for managing slideshow media elements.
   *
   * This behavior is invoked automatically on page load and when new content
   * is added via AJAX. It initializes functionality related to media within a
   * slideshow and ensures proper setup and configuration.
   *
   * Drupal behaviors are designed to enhance modularity and reusability by
   * attaching JavaScript functionality to specific regions or elements within
   * the DOM.
   *
   * Properties:
   * - attach: Defines actions to be taken when this behavior is applied to the document.
   * - detach: Optionally defines how this behavior should clean up when an element
   *   is removed or manipulated.
   *
   * Typical use cases involve the initialization of media components like
   * carousels, lightboxes, or dynamically loaded video embeds.
   */
  Drupal.behaviors.slideshowMedia = {
    /**
     * Slider
     * Mounting function for Splide sliders. Also contains functionality for
     * pagination numbering counter.
     * @param {Object} givenElement - Slider wrapper to act upon.
     */
    slider(givenElement) {
      /* eslint-disable */
      const slideshow = new Splide(givenElement);
      /* eslint-enable */

      slideshow.mount();

      const button = givenElement.querySelector('.splide__toggle');

      if (button) {
        const pausedClass = 'is-paused';

        // Remove the paused class and change the label to "Pause".
        slideshow.on('autoplay:play', () => {
          button.classList.remove(pausedClass);
          button.setAttribute('aria-label', Drupal.t('Pause Autoplay'));
        });

        // Add the paused class and change the label to "Play".
        slideshow.on('autoplay:pause', () => {
          button.classList.add(pausedClass);
          button.setAttribute('aria-label', Drupal.t('Start Autoplay'));
        });
      }

      const playPauseButton = givenElement.querySelector('.splide__toggle');
      if (playPauseButton) {
        const track = givenElement.querySelector('.splide__track');
        const trackId = track.getAttribute('id');
        playPauseButton.setAttribute('aria-controls', trackId);
      }
    },

    /*
     * Attach
     * Final build to the site for display within Drupal context.
     * - @param context: Website document context.
     */

    attach(context) {
      once('slideshowMedia', '.c-slideshow', context).forEach((slider) => {
        if (slider.length === 0) {
          return;
        }
        this.slider(slider);
      });
    },
  };
})(Drupal, once);
