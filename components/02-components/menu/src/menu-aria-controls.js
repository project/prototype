/* eslint-disable */
(function (Drupal) {
  /**
   * Attaches ARIA controls and data attributes to given elements.
   *
   * This function iterates over each element in the provided array and sets the appropriate
   * ARIA attributes based on the element type (button or other).
   *
   * For buttons, it sets `aria-haspopup`, `aria-controls`, `data-menu-controls`, and `aria-label`.
   * For other elements it sets `data-menu-controls`.
   *
   * `data-menu-controls` is an attribute used to support arrow keys in the MenuControl class.
   *
   * @param {HTMLElement[]} elements - An array of HTML elements to which ARIA controls will be attached.
   */
  function attachControls(elements) {
    elements.forEach(function (element) {
      const id = element.getAttribute('data-plugin-id');
      const submenu = element.nextElementSibling;

      if (element.tagName.toLowerCase() === 'button') {
        element.setAttribute('aria-haspopup', 'true');
        if (submenu) {
          const submenuId = `panel-${id}`;
          submenu.setAttribute('id', submenuId);
          element.setAttribute('aria-controls', submenuId);
          element.setAttribute('data-menu-controls', submenuId);
          element.setAttribute('aria-label', element.textContent.trim());
        }
      } else if (submenu) {
        const submenuId = `panel-${id}`;
        submenu.setAttribute('id', submenuId);
        element.setAttribute('data-menu-controls', submenuId);
      }
    });
  }

  /*------------------------------------*\
    - 01 - Drupal Aria & Data Controls
    Attach aria and data controls to menu items
  \*------------------------------------*/
  Drupal.behaviors.ariaControls = {
    attach: function (context) {
      once('ariaControls', '.c-menu', context).forEach(function (menu) {
        // Find top level menu buttons
        const buttons = menu.querySelectorAll(':scope button.menu__link');

        // Find menu spans for megamenu items
        const spans = menu.querySelectorAll(':scope span.menu__link');

        // Attach controls to buttons and spans
        attachControls(buttons);
        attachControls(spans);
      });
    },
  };
})(Drupal);
