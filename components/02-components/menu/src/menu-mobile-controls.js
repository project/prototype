/* eslint-disable */
(function (Drupal) {
  Drupal.behaviors.mobileMenuControls = {
    attach: function (context) {
      once('mobileMenuControls', '.c-menu[data-mobile]', context).forEach(
        (menuContainer) => {
          /**
           * Mobile Menu Controls
           * This attaches the mobile menu controls to the menu container only for menus with
           * the data-mobile attribute. This allows for multiple menus on the same page to have
           * different mobile menu controls.
           *
           * - 01 - Constants
           * - 02 - Functions
           * - 03 - Event Listeners
           * - 04 - Initialize Menus
           */

          /*----------------------------------------------*\
            - 01 - Constants
            Select DOM constants for mobile menu
          \*----------------------------------------------*/
          const mobileNavButtonId = menuContainer
            .getAttribute('data-mobile')
            .replace('#', '');

          // Get the mobileNavButton element using the ID
          const mobileNavButton = document.getElementById(mobileNavButtonId);

          // Default is set in twig template if a custom breakpoint is not set
          const mobileBreakpoint = menuContainer
            .getAttribute('data-breakpoint')
            .replace('#', '');

          // Mobile Media Query
          const mobileMediaQuery = window.matchMedia(
            '(max-width: ' + mobileBreakpoint + 'px)'
          );

          /*------------------------------------------------------------------------------------------------*\
            - 02 - Functions
            closeMobile: Closes the mobile navigation menu, manages focus, and resets dropdown sub-menus.
            mobileControl: Controls the opening and closing of the mobile menu.
          \*------------------------------------------------------------------------------------------------*/
          /**
           * Closes the mobile navigation menu, manages focus, and resets the state of dropdown sub-menus.
           *
           * @param {string} key - The key that was pressed to trigger the function.
           *
           * This function performs the following actions:
           * - Removes the "js-prevent-scroll" class from the body to re-enable scrolling.
           * - Sets the "aria-expanded" attribute of the mobile navigation button to "false" to indicate that the menu is closed.
           * - Resets all dropdown sub-menus within the menu container by setting their "aria-expanded" attribute to "false".
           * - If the "Esc" or "Escape" key was pressed, it sets focus back to the mobile navigation button for accessibility.
           */
          function closeMobile(key) {
            document.body.classList.remove('js-prevent-scroll');
            mobileNavButton.setAttribute('aria-expanded', 'false');

            // Close all dropdown sub-menus
            const menuButtons =
              menuContainer.querySelectorAll('button.menu__link');
            menuButtons.forEach((button) => {
              button.setAttribute('aria-expanded', 'false');
            });

            // It was escape key, set focus
            if (key === 'Esc' || key === 'Escape') {
              mobileNavButton.focus();
            }

            // Remove window listener
            window.removeEventListener('click', onWindowClick);
          }

          /**
           * Controls the behavior of the mobile navigation menu based on user interaction.
           *
           * @param {Event} event - The event that triggered the function.
           *
           * This function checks the "aria-expanded" attribute of the mobile navigation button.
           * If the attribute is "false", the function adds the "js-prevent-scroll" class to the body,
           * and sets the "aria-expanded" attribute of the button to "true".
           * If the attribute is not "false", the function calls the `closeMobile` function to close the menu.
           * The function then stops the propagation of the event and prevents the default action of the event.
           */
          function mobileControl(event) {
            const isMenuClosed =
              mobileNavButton.getAttribute('aria-expanded') === 'false';

            // Toggle overlay
            if (isMenuClosed) {
              document.body.classList.add('js-prevent-scroll');
              mobileNavButton.setAttribute('aria-expanded', 'true');

              // Prevent window click event from closing menu
              event.stopPropagation();
              window.addEventListener('click', onWindowClick);
            } else {
              closeMobile();
            }
          }

          /*------------------------------------------------------------------------------------------------*\
            - 03 - Event Listeners
            handleEscape: Listens for the Escape key to close the mobile navigation menu.
            onWindowClick: Listens for clicks outside the mobile menu and closes if it is open.
          \*------------------------------------------------------------------------------------------------*/

          /**
           * Event listener for keydown events on the window.
           *
           * This function checks if the mobile navigation menu is open and if the Escape key was pressed.
           * If both conditions are true, it calls the `closeMobile` function to close the menu.
           *
           * @param {KeyboardEvent} e - The keydown event.
           */
          const handleEscape = (e) => {
            if (
              mobileNavButton &&
              mobileNavButton.getAttribute('aria-expanded') === 'true' &&
              (e.key === 'Esc' || e.key === 'Escape')
            ) {
              closeMobile('Esc');
            }
          };

          window.addEventListener('keydown', handleEscape);

          /**
           * Closes the mobile navigation menu when a click occurs outside of it.
           *
           * This function is triggered by click events on the window. If on a mobile device and
           * the click event target is not within the menu container, it invokes the `closeMobile` function to close the menu.
           *
           * @param {Event} event - The click event that triggered the function.
           */
          const onWindowClick = (event) => {
            if (
              mobileMediaQuery.matches &&
              !menuContainer.contains(event.target)
            ) {
              closeMobile();
            }
          };

          /*--------------------------------------------*\
            - 04 - Initialize Menus
            Initialize Mobile Controls
          \*--------------------------------------------*/

          mobileNavButton?.addEventListener('click', mobileControl);
        }
      );
    },
  };
})(Drupal);
