/* eslint-disable */
((Drupal) => {
  Drupal.behaviors.menuControl = {
    attach: function (context) {
      once('menuControl', '.c-menu', context).forEach((menuContainer) => {
        /**
         * Accessible Menu
         * This file is based on content that is licensed according to the W3C Software License at
         * https://www.w3.org/copyright/software-license/
         *
         * - 01 - Constants
         * - 02 - MenuLink Class (Base)
         * - 03 - MenuButton Class (Extends MenuLink)
         * - 04 - Initialize Menus
         */

        /*----------------------------------------------*\
            - 01 - Constants
            Select DOM constants for mobile menu
        \*----------------------------------------------*/
        let mobileBreakpoint = 768;

        if (menuContainer.hasAttribute('data-breakpoint')) {
          mobileBreakpoint = menuContainer
            .getAttribute('data-breakpoint')
            .replace('#', '');
        }

        // Mobile Media Query
        mobileMediaQuery = window.matchMedia(
          '(max-width: ' + mobileBreakpoint + 'px)'
        );

        /*------------------------------------------------------------------------*\
            - 02 - MenuLink Class (Base)
            The base class for attaching functions to menu links.
        \*------------------------------------------------------------------------*/

        /**
         * `MenuLinks` is a class that manages the behavior of menu links in a navigation menu.
         * It provides methods to handle keyboard navigation within the menu, including arrow keys, tab, and escape.
         *
         * @class
         * @param {HTMLElement} domNode - The root node of the menu, typically a `<nav>` or `<ul>` element that contains menu items.
         *
         * @property {HTMLElement} domNode - The root node of the menu.
         * @property {Array} menuitemNodes - An array of all menu item nodes in the menu.
         * @property {HTMLElement} firstMenuitem - The first menu item node in the menu.
         * @property {HTMLElement} lastMenuitem - The last menu item node in the menu.
         *
         * @method onMenuitemKeydown - Handles keydown events on menu items.
         * @method handleNestedMenu - Handles navigation in nested menus.
         * @method getNextItem - Finds the next menu__item.
         * @method getPreviousItem - Finds the previous menu__item.
         * @method handleNonNestedMenu - Handles navigation in non-nested menus.
         * @method handleUpArrow - Handles the up arrow key.
         * @method handleDownArrow - Handles the down arrow key.
         * @method handleLeftArrow - Handles the left arrow key.
         * @method handleRightArrow - Handles the right arrow key.
         * @method handleTab - Handles the tab key.
         * @method handleEscape - Handles the escape key.
         */
        class MenuLinks {
          constructor(domNode) {
            this.domNode = domNode;

            //Find parent ul element
            const parentMenu = domNode.closest('ul');

            if (parentMenu) {
              this.menuNode = parentMenu;
              this.menuNodeChildren = Array.from(parentMenu.children);
            } else {
              console.warn('Relative menu not found for element');
            }

            this.menuitemNodes = Array.from(
              domNode.querySelectorAll('.menu__link')
            );
            if (domNode && domNode.nodeName === 'BUTTON') {
              // If it is a button, do nothing
            } else if (this.menuitemNodes.length > 0) {
              this.firstMenuitem = this.menuitemNodes[0];
              this.lastMenuitem =
                this.menuitemNodes[this.menuitemNodes.length - 1];

              this.menuitemNodes.forEach((item) => {
                item.addEventListener(
                  'keydown',
                  this.onMenuitemKeydown.bind(this)
                );
              });
            } else {
              console.warn('No menu items found');
            }
          }

          /**
           * Handles keydown events on menu link items.
           *
           * @param {KeyboardEvent} event - The keydown event.
           * This method checks the key pressed and performs the corresponding action:
           * - Up or ArrowUp: Calls the handleUpArrow method and prevents the default action.
           * - Down or ArrowDown: Calls the handleDownArrow method and prevents the default action.
           * - Left or ArrowLeft: Calls the handleLeftArrow method.
           * - Right or ArrowRight: Calls the handleRightArrow method.
           * - Tab: Calls the handleTab method.
           * - Escape or Esc: Calls the handleEscape method and, if not on mobile, prevents the default action and stops propagation.
           */
          onMenuitemKeydown(event) {
            const key = event.key;
            if (event.ctrlKey || event.altKey || event.metaKey) {
              return;
            }

            switch (key) {
              case 'Up':
              case 'ArrowUp':
                this.handleUpArrow(event.target);
                event.preventDefault();
                break;

              case 'Down':
              case 'ArrowDown':
                this.handleDownArrow(event.target);
                event.preventDefault();
                break;

              case 'Left':
              case 'ArrowLeft':
                this.handleLeftArrow();
                break;

              case 'Right':
              case 'ArrowRight':
                this.handleRightArrow();
                break;

              case 'Tab':
                this.handleTab(event);
                break;

              case 'Escape':
              case 'Esc':
                this.handleEscape();

                // Stop propagation and prevent default for submenus
                if (this.menuNode.dataset.depth != 0) {
                  event.stopPropagation();
                  event.preventDefault();
                }
                break;

              default:
                break;
            }
          }

          /*----------------------------------------------*\
            Supporting functions Keydown for MenuLinks
          \*----------------------------------------------*/

          /**
           * Handles the nested menu based on the direction of navigation.
           *
           * @param {string} direction - The direction of navigation ("right" or "left").
           *
           * This method finds the parent menu item of the current active menu item and,
           * based on the direction of navigation, determines the next or previous sibling menu item.
           */
          handleNestedMenu(direction) {
            let menuNode = this.menuNode;
            let menuController = menuContainer.querySelector(
              `[data-menu-controls="${menuNode.id}"]`
            );

            if (direction === 'left' && menuNode.dataset.depth >= '2') {
              // Close parent menu item and reset focus
              // If parent menu is interactive, e.g. button or link, close it
              if (
                (menuController && menuController?.tagName === 'BUTTON') ||
                menuController?.tagName === 'A'
              ) {
                menuController.click();
                menuController.focus();
                return;
              }
            }

            // Find first nested menu
            while (menuNode && menuNode.dataset.depth !== '1') {
              menuNode = menuNode.parentElement;
            }

            // Knowing nested menu, set updated controller item
            menuController = menuContainer.querySelector(
              `[data-menu-controls="${menuNode.id}"]`
            );
            const controllerItem = menuController.closest('.menu__item');

            // Find top level menu
            let topMenu = menuNode;
            while (topMenu && topMenu.dataset.depth !== '0') {
              topMenu = topMenu.parentElement;
            }

            // Get all top level menu items
            const menuItems = Array.from(topMenu.children).filter((child) =>
              child.matches('.menu__item')
            );

            // Find the index of the menuController in array of items
            const targetIndex = menuItems.indexOf(controllerItem);

            const leftSibling = this.getPreviousItem(menuItems, targetIndex);
            const rightSibling = this.getNextItem(menuItems, targetIndex);

            // Determine which sibling to focus based on direction
            let siblingToFocus =
              direction === 'right' ? rightSibling : leftSibling;

            // Close open buttons
            this.closeAllButtons();

            // Select actual focus element
            siblingToFocus = siblingToFocus.querySelector('.menu__link');
            siblingToFocus.focus();
          }

          /**
           * Checks if the current item is the last item in the menu.
           *
           * @param {HTMLElement[]} menuItems - An array of menu items.
           * @param {number} targetIndex - The index of the current item in the menuItems array.
           * @returns {boolean} - Returns true if the current item is the last item in the menu, otherwise false.
           */
          isLastItem(menuItems, targetIndex) {
            return targetIndex + 1 >= menuItems.length;
          }

          /**
           * Retrieves all focusable elements within a menu.
           *
           * @param {HTMLElement} menu - The menu ul element.
           *
           * @returns {HTMLElement[]} - An array of focusable elements within the menu.
           */
          getFocusableElements(menu) {
            return Array.from(
              menu.querySelectorAll('.menu__link:is(a[href], button)')
            ).filter((element) =>
              element.checkVisibility({
                opacityProperty: true,
                visibilityProperty: true,
              })
            );
          }

          /**
           * Retrieves the next item in a menu based on the current target index.
           * If the target index is the last item, it returns the first item,
           * effectively treating the menu as a circular array.
           *
           * @param {Array} menuItems - An array of menu items.
           * @param {number} targetIndex - The current index in the menu items array.
           * @returns The next item in the menu. If the target index is the last item, returns the first item in the array.
           */
          getNextItem(menuItems, targetIndex) {
            return targetIndex + 1 < menuItems.length
              ? menuItems[targetIndex + 1]
              : menuItems[0];
          }
          /**
           * Retrieves the previous item in a menu based on the current target index.
           * If the target index is the first item (0), it returns the last item,
           * effectively treating the menu as a circular array.
           *
           * @param {Array} menuItems - An array of menu items.
           * @param {number} targetIndex - The current index in the menu items array.
           * @returns The previous item in the menu. If the target index is 0, returns the last item in the array.
           */
          getPreviousItem(menuItems, targetIndex) {
            if (targetIndex - 1 >= 0) {
              // If there is a previous item, return it
              return menuItems[targetIndex - 1];
            } else {
              // If targetIndex is 0, return the last item of the array
              return menuItems[menuItems.length - 1];
            }
          }

          /**
           * Handles the navigation for non-nested menus based on the direction of navigation.
           *
           * @param {HTMLElement} target - The current active menu item.
           * @param {string} direction - The direction of navigation ("right" or "left").
           *
           * This method finds the next or previous sibling menu item of the current active menu item based on the direction.
           * If such a sibling menu item exists and it contains a link, it focuses on that link.
           * Otherwise, it focuses on the first menu item link in the parent menu (or the last one if the direction is "left").
           */
          handleNonNestedMenu(direction) {
            let menuItems = this.menuNodeChildren;

            // Find the index of the target in this.menuNodeChildren
            const targetIndex = menuItems.indexOf(
              this.domNode.closest('.menu__item')
            );

            const leftSibling = this.getPreviousItem(menuItems, targetIndex);
            const rightSibling = this.getNextItem(menuItems, targetIndex);

            // Determine which sibling to focus based on direction
            let siblingToFocus =
              direction === 'right' ? rightSibling : leftSibling;

            // Select actual focus element
            siblingToFocus = siblingToFocus.querySelector('.menu__link');
            siblingToFocus.focus();
          }

          /**
           * Sets focus to the first `.menu__link` within the next sibling `ul` element.
           * If no next sibling `ul` element is found, sets focus to the first `.menu__link` within the parent menu.
           *
           * @param {HTMLElement} menuNode - The current menu node.
           * @param {HTMLElement} parentMenu - The parent menu element.
           */
          nextExpandedMenu(menuNode, parentMenu) {
            const menuParent = menuNode.parentElement;
            const nextParent = menuParent.nextElementSibling;

            if (nextParent) {
              const nextLink = nextParent.querySelector(
                '.menu__link:is(a[href], button)'
              );
              if (nextLink) {
                nextLink.focus();
              }
            } else {
              const firstLink = parentMenu.querySelector(
                '.menu__link:is(a[href], button)'
              );
              firstLink.focus();
            }
          }

          /**
           * Sets focus to the last `.menu__link` within the previous sibling `ul` element.
           * If no previous sibling `ul` element is found, sets focus to the last `.menu__link` within the parent menu.
           *
           * @param {HTMLElement} menuNode - The current menu node.
           * @param {HTMLElement} parentMenu - The parent menu element.
           */
          previousExpandedMenu(menuNode, parentMenu) {
            const menuParent = menuNode.parentElement;
            const previousParent = menuParent.previousElementSibling;

            if (previousParent) {
              // Find the last link in the previous menu.
              const menuLinks = previousParent.querySelectorAll(
                '.menu__link:is(a[href], button)'
              );
              menuLinks[menuLinks.length - 1].focus();
            } else {
              const menuItems = parentMenu.querySelectorAll('.menu__item');

              // Find last menu link within last menuItems.
              const menuLinks = menuItems[
                menuItems.length - 1
              ].querySelectorAll('.menu__link:is(a[href], button)');
              // Set focus to last index of menuLinks.
              menuLinks[menuLinks.length - 1].focus();
            }
          }

          /*------------------------------------*\
              Keydown functions for MenuLinks
          \*------------------------------------*/

          /**
           * Handles the 'Up Arrow' key event for a menu item.
           *
           * @param {HTMLElement} target - The current active menu item.
           *
           * This method finds the previous sibling menu item of the current active menu item.
           * If such a sibling menu item exists and it contains a link, it focuses on that link.
           * Otherwise, it finds the closest parent 'ul' element, gets all its direct child menu item links,
           * and focuses on the last one.
           */
          handleUpArrow(target) {
            const menuDepth = this.menuNode.dataset.depth;
            // If top level menu, do nothing.
            if (menuDepth == 0) return;

            const rootMenu = target.closest(
              'button.menu__link[aria-expanded="true"] + .c-menu__list'
            );
            const focusableElements = this.getFocusableElements(rootMenu);

            if (
              focusableElements.length > 0 &&
              focusableElements.indexOf(target) >= 0
            ) {
              const index = focusableElements.indexOf(target);
              const prev =
                index === 0
                  ? focusableElements[focusableElements.length - 1]
                  : focusableElements[index - 1];

              prev.focus();
            }
          }

          /**
           * Handles the 'Down Arrow' key event for a menu item.
           *
           * @param {HTMLElement} target - The current active menu item.
           *
           * This method finds the next sibling menu item of the current active menu item.
           * If such a sibling menu item exists and it contains a link, it focuses on that link.
           * Otherwise, it finds the closest parent 'ul' element, looks for a sibling linkgets all its direct child menu item links,
           * and focuses on the last one.
           */
          handleDownArrow(target) {
            const menuDepth = this.menuNode.dataset.depth;
            // If we are at the top level and the target is a link, do nothing.
            if (menuDepth == 0 && target?.tagName === 'A') return;

            const rootMenu = target.closest(
              'button.menu__link[aria-expanded="true"] + .c-menu__list'
            );

            // Focusable elements are links or buttons that are visible.
            const focusableElements = this.getFocusableElements(rootMenu);

            if (
              focusableElements.length > 0 &&
              focusableElements.indexOf(target) >= 0
            ) {
              const index = focusableElements.indexOf(target);
              const next =
                index + 1 < focusableElements.length
                  ? focusableElements[index + 1]
                  : focusableElements[0];

              next.focus();
            }
          }

          /**
           * Handles the 'Left Arrow' key event for a menu item.
           *
           * @param {HTMLElement} target - The current active menu item.
           *
           * This method checks if the parent menu of the current active menu item is a nested menu with data-depth > 0.
           * If it is, it calls the `handleNestedMenu` method with "left" as an argument.
           * Otherwise, it calls the `handleNonNestedMenu` method with "left" as an argument.
           */
          handleLeftArrow() {
            const parentMenu = this.menuNode;
            // Check if the parent menu is a nested menu with data-depth > 0
            if (parentMenu.dataset.depth && parentMenu.dataset.depth > 0) {
              this.handleNestedMenu('left');
            } else {
              this.handleNonNestedMenu('left');
            }
          }

          /**
           * Handles the 'Right Arrow' key event for a menu item.
           *
           * @param {HTMLElement} target - The current active menu item.
           *
           * This method determines the type of the parent menu of the current active menu item based on its data-depth attribute.
           * If the parent menu is a nested menu (data-depth > 0), it delegates the handling to the `handleNestedMenu` method.
           * If the parent menu is not a nested menu (data-depth <= 0), it delegates the handling to the `handleNonNestedMenu` method.
           * Both `handleNestedMenu` and `handleNonNestedMenu` methods are called with "right" as an argument.
           */
          handleRightArrow() {
            const parentMenu = this.menuNode;
            // Check if the parent menu is a nested menu with data-depth > 0
            if (parentMenu.dataset.depth && parentMenu.dataset.depth > 0) {
              this.handleNestedMenu('right');
            } else {
              this.handleNonNestedMenu('right');
            }
          }

          /**
           * Handles the 'Tab' key event for a menu item.
           *
           * @param {Event} event - The key event.
           * @param {HTMLElement} target - The current active menu item.
           *
           * Preconditions:
           * - The method is called within the context of a nested menu (depth > 0).
           * - The target menu item is the first child of its parent menu.
           *
           * Actions:
           * - If the above conditions are met, the method simulates a click on the menu controller button (if it is indeed a button), which is expected to toggle the visibility of the menu, typically closing it.
           */
          handleTab(event) {
            // Find the parent menuNode with dataset.depth === '1' for nested menus
            let menuNode = this.menuNode;
            while (menuNode && menuNode.dataset.depth !== '1') {
              menuNode = menuNode.parentElement;
            }

            // If within a nested menu and tabbing only
            if (menuNode && !event.shiftKey) {
              // Directly select direct .menu__item children
              const menuLinkNodes = menuNode.querySelectorAll(
                ':scope > .menu__item'
              );
              const allMenuLinks = menuNode.querySelectorAll('.menu__item');

              const lastItem = menuLinkNodes[menuLinkNodes.length - 1];
              const lastNestedItem = allMenuLinks[allMenuLinks.length - 1];

              // Check if the last item is the target's parent and close the menu if true
              if (
                lastItem === this.domNode.closest('.menu__item') ||
                lastNestedItem === this.domNode.closest('.menu__item')
              ) {
                this.closeAllButtons();
              }
            }

            // If user shift+tabs into first item, close menu
            if (event.shiftKey && this.menuNode.dataset.depth > 0) {
              const menuController = menuContainer.querySelector(
                `[data-menu-controls="${this.menuNode.getAttribute('id')}"]`
              );

              const firstItem =
                this.menuNodeChildren[0] ===
                this.domNode.closest('.menu__item');

              if (firstItem && menuController?.tagName === 'BUTTON') {
                menuController.click();
              }
            }
          }

          /**
           * Handles the 'Escape' key event for a menu item.
           *
           * This method locates the button that controls the current menu (identified by the 'data-menu-controls' attribute matching the menu's ID).
           * If such a button is found, it performs two actions:
           * - Sets the button's 'aria-expanded' attribute to 'false', effectively closing the menu.
           * - Moves focus to the button, facilitating keyboard navigation back to the menu controller.
           *
           * Note: This method assumes the controlling button is associated with the menu via the 'data-menu-controls' attribute.
           */
          handleEscape() {
            const menuDepth = this.menuNode.dataset.depth;
            let currentMenu = this.menuNode;

            // Check if the current menu is expanded.
            let menuController = menuContainer.querySelector(
              `[data-menu-controls="${currentMenu.getAttribute(
                'id'
              )}"][aria-expanded="true"]`
            );

            // Otherwise find the closest expanded menu.
            if (!menuController && menuDepth > 1) {
              while (currentMenu && currentMenu.dataset.depth !== '1') {
                currentMenu = currentMenu.parentElement;
              }

              // Find controller button for menuNode with aria-control id
              menuController = menuContainer.querySelector(
                `[data-menu-controls="${currentMenu.getAttribute('id')}"]`
              );
            }

            if (menuController) {
              menuController.setAttribute('aria-expanded', 'false');
              menuController.focus();
            }
          }

          /**
           * Closes all menu buttons except for the current one.
           *
           * This method finds all buttons within the menu that have their `aria-expanded` attribute set to `true`,
           * indicating that their associated submenu is open. It then proceeds to close all these menus by setting
           * their `aria-expanded` attribute to `false`. It does this for both top-level menu buttons and buttons in
           * submenus, ensuring that all other menus are closed except for the menu associated with the current
           * `domNode` (the menu button that invoked this method).
           */
          closeAllButtons() {
            const topButtons = Array.from(
              menuContainer.querySelectorAll(
                '[data-depth="0"] > li > button[aria-expanded="true"]'
              )
            );
            const menuNodeButtons = Array.from(
              menuContainer.querySelectorAll('button[aria-expanded="true"]')
            );

            topButtons
              .filter((button) => button !== this.domNode)
              .forEach((button) => {
                button.setAttribute('aria-expanded', 'false');
              });

            // Close all buttons in submenus
            menuNodeButtons
              .filter((button) => button !== this.domNode)
              .forEach((button) => {
                button.setAttribute('aria-expanded', 'false');
              });
          }
        }

        /*---------------------------------------------------------------*\
            - 03 - MenuButton Class (Extends MenuLink)
            The base class for attaching functions to menu controls.
        \*----------------------------------------------------------------*/
        /**
         * `MenuButton` is a class that extends `MenuLinks` to handle the functionality of menu controls.
         *
         * @extends MenuLinks
         *
         * @property {HTMLElement} buttonNode - The button element in the menu.
         * @property {HTMLElement} menuNode - The related ul menu for the button.
         * @property {Array} menuitemNodes - The menu items in the menu.
         * @property {HTMLElement} firstMenuitem - The first menu item in the menu.
         * @property {HTMLElement} lastMenuitem - The last menu item in the menu.
         *
         * The constructor initializes the base `MenuLinks` class with the main menu, sets the `aria-expanded` attribute of the button to 'false',
         * and attaches event listeners to the main menu button, each menu item, and the window.
         *
         * The class includes methods to handle button and menu item keydown events, button click events, and background mousedown events,
         * as well as methods to open and close the menu, check if the menu is open, and focus on the first menu item.
         */
        class MenuButton extends MenuLinks {
          constructor(menuContainer) {
            // Initialize the base MenuLinks class with the main menu
            super(menuContainer);

            this.buttonNode = menuContainer;
            this.buttonNode.setAttribute('aria-expanded', 'false');

            // Attach event listeners to the main menu button
            this.buttonNode.addEventListener(
              'keydown',
              this.onButtonKeydown.bind(this)
            );
            this.buttonNode.addEventListener(
              'click',
              this.onButtonClick.bind(this)
            );

            // Attach event listeners to each menu item
            this.menuitemNodes.forEach((menuitem, index) => {
              menuitem.addEventListener(
                'keydown',
                this.onMenuitemKeydown.bind(this)
              );

              if (index === 0) {
                this.firstMenuitem = menuitem;
              }
              this.lastMenuitem = menuitem;
            });

            window.addEventListener(
              'mousedown',
              this.onBackgroundMousedown.bind(this),
              true
            );
          }

          /**
           * Focuses on the first menu item of the nested list related to the given element.
           *
           * @param {HTMLElement} element - The element related to the nested list.
           *
           * This method finds the nested list related to the given element by using the `data-menu-controls` attribute.
           * If the nested list exists, it finds the first menu item in the list and focuses on it.
           * If the first menu item is not a button or link, it recursively calls itself to focus on the first menu item within the nested menu.
           */
          focusFirstItem(element) {
            const nestedList = document.getElementById(
              element.getAttribute('data-menu-controls')
            );

            if (nestedList) {
              const firstItem = nestedList.querySelector('.menu__link');
              // If the first item is not a button, it is a default menu so move to the first menu item within.
              if (firstItem.tagName !== 'BUTTON' && firstItem.tagName !== 'A') {
                this.focusFirstItem(firstItem);
              } else {
                firstItem.focus();
              }
            }
          }

          /**
           * Handles keydown events on the menu button.
           *
           * @param {Event} event - The keydown event.
           *
           * This method checks the key pressed during the event and performs actions based on the key:
           * - 'Up' or 'ArrowUp': Calls the `handleUpArrow` method with the button node.
           * - 'Down' or 'ArrowDown': If the next sibling of the button node has a 'data-depth' attribute of '1', opens the popup, focuses on the first menu item, and prevents the default action. Otherwise, calls the `handleDownArrow` method with the button node and prevents the default action.
           * - 'Left' or 'ArrowLeft': If the next sibling of the button node does not have a 'data-depth' attribute of '1', closes the popup and focuses on the button node. Otherwise, calls the `handleLeftArrow` method with the button node.
           * - 'Right' or 'ArrowRight': If the next sibling of the button node does not have a 'data-depth' attribute of '1', opens the popup and focuses on the first menu item. Otherwise, calls the `handleRightArrow` method with the button node.
           * - 'Esc' or 'Escape': Closes the popup and prevents the default action.
           *
           * If the 'ctrl', 'alt', or 'meta' key is pressed during the event, the method returns without doing anything.
           */
          onButtonKeydown(event) {
            const key = event.key;
            if (event.ctrlKey || event.altKey || event.metaKey) {
              return;
            }

            const relatedMenu = document.getElementById(
              this.buttonNode.getAttribute('data-menu-controls')
            );

            switch (key) {
              case 'Tab':
                this.handleTab(event);
                break;

              case 'Up':
              case 'ArrowUp':
                this.handleUpArrow(event.target);
                break;

              case 'Down':
              case 'ArrowDown':
                // Second level menu opens with down arrow
                if (relatedMenu.dataset.depth == '1') {
                  this.openPopup();
                  this.focusFirstItem(event.target);
                  event.preventDefault();
                } else {
                  this.handleDownArrow(event.target);
                  event.preventDefault();
                }
                break;

              case 'Left':
              case 'ArrowLeft':
                this.handleLeftArrow();

                break;

              case 'Right':
              case 'ArrowRight':
                // Deeply nested menus open with right arrow
                if (relatedMenu.dataset.depth !== '1') {
                  this.openPopup();
                  this.focusFirstItem(event.target);
                } else {
                  this.handleRightArrow();
                }
                break;

              case 'Esc':
              case 'Escape':
                this.closePopup();
                event.preventDefault();
                break;
            }
          }

          /**
           * Handles click events on the menu button.
           *
           * @param {Event} event - The click event.
           *
           * This method checks if the menu is open:
           * - If the menu is open, it calls the `closePopup` method to close the menu.
           * - If the menu is not open, it calls the `openPopup` method to open the menu.
           *
           * After handling the menu, it stops the propagation of the event and prevents the default action.
           */
          onButtonClick(event) {
            if (this.isOpen()) {
              this.closePopup();
            } else {
              this.openPopup();
              // Only close other buttons if not on mobile
              if (!mobileMediaQuery.matches) {
                this.closeAll();
              }
            }

            event.stopPropagation();
            event.preventDefault();
          }

          /**
           * Checks if the menu is open.
           *
           * @returns {boolean} - Returns true if the menu is open, false otherwise.
           *
           * This method checks the 'aria-expanded' attribute of the button node.
           * If the attribute is 'true', the method returns true, indicating that the menu is open.
           * If the attribute is not 'true', the method returns false, indicating that the menu is not open.
           */
          isOpen() {
            return this.buttonNode.getAttribute('aria-expanded') === 'true';
          }

          /**
           * Opens the popup menu.
           *
           * This method sets the 'aria-expanded' attribute of the button node to 'true',
           * indicating that the associated popup menu is open.
           */
          openPopup() {
            this.buttonNode.setAttribute('aria-expanded', 'true');
          }

          /**
           * Closes the popup menu.
           *
           * This method sets the 'aria-expanded' attribute of the button node to 'false',
           * indicating that the associated popup menu is closed.
           */
          closePopup() {
            this.buttonNode.setAttribute('aria-expanded', 'false');
          }

          /**
           * Handles the mousedown event on the background.
           *
           * @param {Event} event - The mousedown event.
           *
           * This method checks if the mousedown event occurred outside the menu container.
           * If it did and the menu is open and the device is not mobile, it sets focus to the button node and closes the popup menu.
           */
          onBackgroundMousedown(event) {
            if (!menuContainer.contains(event.target)) {
              if (this.isOpen() && !mobileMediaQuery.matches) {
                this.buttonNode.focus();
                this.closePopup();
              }
            }
          }

          /**
           * Closes all expanded buttons within the menu, with specific behavior based on the menu's depth.
           * - If the menu is at the top level (depth 0), it closes all top-level buttons except for `this.buttonNode`.
           * - For menus not at the top level, it ensures that only one button can be open at a time by closing all other buttons except for `this.buttonNode`.
           * This method is part of the menu management functionality, allowing for better accessibility and user experience by managing the expanded state of menu buttons.
           */
          closeAll() {
            const topButtons = menuContainer.querySelectorAll(
              '[data-depth="0"] > li > button[aria-expanded="true"]'
            );

            switch (this.menuNode.dataset.depth) {
              // Close all buttons if top-level
              case '0':
                topButtons.forEach((button) => {
                  if (button !== this.buttonNode) {
                    // Close the top-level button
                    button.setAttribute('aria-expanded', 'false');
                  }
                });
                // Find and close all nested buttons within this top-level button
                Array.from(
                  this.menuNode.querySelectorAll('button[aria-expanded="true"]')
                )
                  .filter((button) => button !== this.buttonNode)
                  .forEach((button) => {
                    button.setAttribute('aria-expanded', 'false');
                  });
                break;
              default:
                //  Only allow one button to be open at a time inside a menu
                Array.from(
                  this.menuNode.querySelectorAll('button[aria-expanded="true"]')
                )
                  .filter((button) => button !== this.buttonNode)
                  .forEach((button) => {
                    button.setAttribute('aria-expanded', 'false');
                  });

                break;
            }
          }
        }

        /*--------------------------------------------*\
            - 04 - Initialize Menus
            Initialize Menus for keyboard navigation
        \*--------------------------------------------*/

        /**
         * Initializes the menus within a given container.
         *
         * @param {HTMLElement} menuContainer - The container within which to initialize menus.
         *
         * This function selects all button elements within the menuContainer and initializes a new MenuButton instance for each.
         * It then selects all elements with the class 'menu__item' within the menuContainer.
         * For each 'menu__item' that does not contain a button, it initializes a new MenuLinks instance.
         */
        function initializeMenus(menuContainer) {
          // Initialize MenuButton for each button in the menuContainer.
          menuContainer
            .querySelectorAll('button.menu__link')
            .forEach((button) => {
              new MenuButton(button);
            });

          // Initialize main menu list
          menuContainer
            .querySelectorAll(
              '.menu__item:not(.menu__item--expanded:has(> span.menu__link))'
            )
            .forEach((item) => {
              if (item.querySelector('button') === null) {
                new MenuLinks(item);
              }
            });
        }

        // Call the function with the menuContainer as argument
        initializeMenus(menuContainer);
      });
    },
  };
})(Drupal);
