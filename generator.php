<?php

/**
 * @file
 * Prototype subtheme generator.
 *
 * This file allows users to create custom subthemes with Prototype
 * supported by Drupal core's starterkit generator.
 *
 * PHP version 8.1
 *
 * @package Prototype
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link https://www.drupal.org/docs/core-modules-and-themes/core-themes/starterkit-theme
 * @see Drupal Core Starterkit: https://www.drupal.org/about/core/blog/new-starterkit-will-change-how-you-create-themes-in-drupal-10
 * @since File available since Release 4.0.0-alpha
 */
namespace Prototype;

// Define the help message.
$description = "Description:
  Generates a new theme based on the latest Prototype codebase.\n";

$usage = "Usage:
  generator.php [options] [--] <machine-name>
  generator.php --name custom_theme --display-name 'Custom Theme' --path themes --abbreviated short\n";

$varOptions = "Options:
 -n, --name           A name for the theme.
 -d, --display-name   A human readable display name for your theme.
 -p, --path           The path where your theme will be created. Defaults to: themes/custom
 -a, --abbreviated    Use abbreviated theme name for CSS variables.
 \n";

// Parse command-line arguments.
$shortopts = 'h';
$longopts = ['help'];
$options = getopt($shortopts, $longopts);
// If the help option was passed, display the help message and exit.
if (isset($options['help']) || isset($options['h'])) {
  echo $description . "\n" . $usage . "\n" . $varOptions . "\n";
  exit(0);
}

// Default values.
$theme_name = 'prototype_subtheme';
$theme_path = 'themes/custom';
$theme_display_name = '';
$theme_abbr = '';

// Parse command-line arguments.
$shortopts = 'n:p:d:a:';
$longopts = ['name:', 'path:', 'display-name:', 'abbreviated:'];
$options = getopt($shortopts, $longopts);

/**
 * Sanitize provided theme name.
 *
 * This function cleans provided name to meet Drupal expectations.
 *
 * @param string $data
 *   - Theme name provided as string.
 */
function sanitize_theme_name($data) {
  $cleaned_string = preg_replace("/[^A-Za-z0-9\_]/", "", $data);

  // If sanitized string is empty, stop generator.
  if (empty($cleaned_string)) {
    echo ("An invalid name was provided, please try again.\n");
    die();
  }

  return $cleaned_string;
}

/**
 * Sanitize provided theme path.
 *
 * @param string $data
 *   - Folder path provided as string.
 */
function sanitize_path($data) {
  // Remove forward & training slashes they exist.
  $string = preg_replace_callback('/\/([A-Za-z0-9]+)[\s-]*([A-Za-z0-9]*)/', function ($matches) {
    return '/' . str_replace([' ', '-'], '', $matches[1]) .
      str_replace([' ', '-'], '', $matches[2]);
  }, $data);
  $cleaned_string = preg_replace('/^\/|\/$/', '', $string);

  // If sanitized string is empty, stop generator.
  if (empty($cleaned_string)) {
    echo ("An invalid name was provided, please try again.\n");
    die();
  }

  return $cleaned_string;
}

/**
 * Sanitize provided theme display name.
 *
 * This function cleans provided name to meet Drupal expectations.
 *
 * @param string $data
 *   - Theme name provided as string.
 */
function sanitize_theme_display_name($data) {
  $cleaned_string = preg_replace("/[^A-Za-z0-9 \_]/", "", $data);

  // If sanitized string is empty, stop generator.
  if (empty($cleaned_string)) {
    echo ("An invalid name was provided, please try again.\n");
    die();
  }

  return $cleaned_string;
}

/**
 * Replace all CSS variable names with abbreviation.
 *
 * @param string $dir
 *   - Directory to update files within.
 * @param string $theme_abbr
 *   - Provided machine abbreviation for theme variables.
 */
function update_property_prefix($dir, $theme_abbr) {
  // Find existing files within directories.
  $iterator = new \RecursiveIteratorIterator(
    new \RecursiveDirectoryIterator($dir)
  );

  // Loop through directories and update needed files.
  foreach ($iterator as $file) {
    if ($file->isFile() && is_readable($file)) {
      $file_contents = file_get_contents($file);

      // Should we even try to update the file.
      if (preg_match("/property-prefix: '(.*)'/", $file_contents, $matches)) {
        $file_contents = preg_replace("/property-prefix: '(.*)'/", "property-prefix: '$theme_abbr'", $file_contents);

        // Validate if we can write to this file.
        if (!is_writable($file)) {
          echo 'There was an issue saving ' . $file;
          return;
        }
        file_put_contents($file, $file_contents);
      }
    }
  }
}

/**
 * Replace all CSS variable names with abbreviation.
 *
 * @param SplFileInfo $file
 *   - The file to update.
 * @param string $theme_name
 *   - New theme name as this runs after Drupal's generator.
 * @param string $theme_abbr
 *   - Provided machine abbreviation for theme variables.
 */
function update_variables($file, $theme_name, $theme_abbr) {
  if ($file->isFile() && is_readable($file->getPathname())) {
    $file_contents = file_get_contents($file->getPathname());

    if (str_contains($file_contents, "--$theme_name")) {
      $file_contents = str_replace("--$theme_name", "--$theme_abbr", $file_contents);

      // Validate if we can write to this file.
      if (!is_writable($file->getPathname())) {
        echo 'There was an issue saving ' . $file;
        return;
      }
      file_put_contents($file->getPathname(), $file_contents);
    }
  }
}

/**
 * Replace all CSS variable names with abbreviation.
 *
 * @param string $dir
 *   - Directory to update files within.
 * @param string $theme_name
 *   - New theme name as this runs after Drupal's generator.
 * @param string $theme_abbr
 *   - Provided machine abbreviation for theme variables.
 */
function update_dir_variables($dir, $theme_name, $theme_abbr) {
  // Find existing files within directories.
  $iterator = new \RecursiveIteratorIterator(
    new \RecursiveDirectoryIterator($dir)
  );

  // Loop through directories and update needed files.
  foreach ($iterator as $file) {
    update_variables($file, $theme_name, $theme_abbr);
  }
}

// Set values based on command-line arguments or defaults.
if (isset($options['n']) || isset($options['name'])) {
  $theme_name = $options['n'] ?? $options['name'];
}
else {
  // Store desired subtheme name.
  $theme_name = readline('Enter subtheme machine name: ');
}

// Santaize data.
$theme_name = sanitize_theme_name($theme_name);

// Would user like to change install path.
if (isset($options['p']) || isset($options['path'])) {
  $changePath = $options['p'] ?? $options['path'];
}
else {
  $changePath = readline("Where should we generate the subtheme? [ $theme_path ]: ");
}
// If user entered data sanitize, otherwise use default.
$theme_path = empty($changePath) ? $theme_path : sanitize_path($changePath);


if (isset($options['d']) || isset($options['display-name'])) {
  $theme_display_name = $options['d'] ?? $options['display-name'];
}
else {
  // Would user like to set a name for the theme.
  $theme_display_name = readline("Set a theme name: ");
}

// If user provided a name, sanitize and set flag.
if (!empty($theme_display_name)) {
  $theme_display_name = sanitize_theme_display_name($theme_display_name);

  // Attach flag.
  $theme_display_name = " --name '" . $theme_display_name . "'";
}

if (isset($options['a']) || isset($options['abbreviated'])) {
  $theme_abbr = $options['a'] ?? $options['abbreviated'];
}
else {
  // Would user like to set an abbreviated name for the theme.
  $tempName = readline("Would you like to set an abbreviated name: ");
  // If user entered data sanitize, otherwise use default.
  $theme_abbr = empty($tempName) ? $theme_abbr : sanitize_theme_name($tempName);
}

// If user provided a abbreviated name, sanitize and set flag.
if (!empty($theme_abbr)) {
  $theme_abbr = sanitize_theme_name($theme_abbr);
}

// Change directory, check if the custom folder exist.
// Confirm if the custom folder is in the requested path.
chdir('../..');
if (!is_dir('custom') && str_contains($theme_path, 'custom')) {
  // If missing add directory.
  mkdir('custom');
}

// Run Drupal Core command.
chdir('../');
echo "Generating your theme now, please wait. \n";

// Build command with updated variables.
$build_command = $theme_name . $theme_display_name . " --path "
  . $theme_path . " --starterkit prototype";

// Run Drupal core starterkit script.
exec("php core/scripts/drupal generate-theme " . $build_command . " -n ", $output, $return);

// Did we create the theme successfully.
if ($return != 0) {
  echo "An error occurred generating the theme. \n";
  die();
}
else {
  // Display success message.
  echo $output[0] . "\n";
}

// Navigate to new theme and continue.
chdir($theme_path . "/" . $theme_name);

// If the user passed $theme_abbr.
if (!empty($theme_abbr)) {
  // Only include dedicated style directories.
  $style_directories = ['partials', 'components'];
  $prefix = str_replace('_', '-', $theme_abbr);
  // Replace CSS variables with new theme name.
  foreach ($style_directories as $item) {
    update_property_prefix($item, $prefix);
    update_dir_variables($item, $theme_name, $prefix);
  }

  // Update the newly created README file.
  update_variables(new \SplFileInfo('README.md'), $theme_name, $prefix);
}
